package com.knasirayaz.cytique.printerservice;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class PrintService {
    public static final String CHINESE = "GBK";

    BluetoothAdapter mBluetoothAdapter;
    private String mConnectedDeviceName = null;
    private Context mContext;
    private BluetoothService mService = null;
    private CustomBluetoothService mCustomBluetoothService = null;


    public PrintService(Context mContext, BluetoothService mService, BluetoothAdapter mBluetoothAdapter) {
        this.mContext = mContext;
        this.mService = mService;
        this.mBluetoothAdapter = mBluetoothAdapter;
    }

    public PrintService(Context mContext, CustomBluetoothService mCustomBluetoothService, BluetoothAdapter mBluetoothAdapter) {
        this.mContext = mContext;
        this.mCustomBluetoothService = mCustomBluetoothService;
        this.mBluetoothAdapter = mBluetoothAdapter;
    }

    public PrintService(Context mContext, BluetoothAdapter mBluetoothAdapter) {
        this.mContext = mContext;
        this.mBluetoothAdapter = mBluetoothAdapter;
    }

    public void initializzePrintService(Context mContext, CustomBluetoothService mCustomBluetoothService, BluetoothAdapter mBluetoothAdapter) {
        this.mContext = mContext;
        this.mCustomBluetoothService = mCustomBluetoothService;
        this.mBluetoothAdapter = mBluetoothAdapter;
    }

    public void printNow(PrintModel mPrintModel, int numberOfPrints, final IPrinterFinishedTask finishedTask) {
        int largerFont = 1;
        int smallerFont = 0;
        int fontSize = 0;

        String header1 = "\b  " + "-----------------" + "   \b";
        String header = "\b " + "ARY Sahulat Wallet" + "  \b";
        String header2 = "\b " + "-----------------" + "   \b\n";
        String bar1 = "-----------------------------\n";
        String recieptType = "\b         " + "Merchant Copy" + "   \b \n";
        String bar2 = "-----------------------------\n";


        SendDataByte(PrinterCommand.POS_Print_Text(header1, CHINESE, 0, fontSize, fontSize, fontSize));
        SendDataByte(PrinterCommand.POS_Print_Text(header, CHINESE, 0, fontSize, fontSize, fontSize));
        SendDataByte(PrinterCommand.POS_Print_Text(header2, CHINESE, 0, fontSize, fontSize, fontSize));


        for (int i = 0; i < 3; i++) {

            if (mPrintModel.getPrintModel().get(i).getFontSize().equals(largerFont)) {
                fontSize = largerFont;
            } else {
                fontSize = smallerFont;
            }

            SendDataByte(PrinterCommand.POS_Print_Text(header, CHINESE, 0, fontSize, fontSize, fontSize));

        }
        SendDataByte(PrinterCommand.POS_Print_Text(bar1, CHINESE, 0, fontSize, fontSize, fontSize));
        SendDataByte(PrinterCommand.POS_Print_Text(recieptType, CHINESE, 0, fontSize, fontSize, fontSize));
        SendDataByte(PrinterCommand.POS_Print_Text(bar2, CHINESE, 0, fontSize, fontSize, fontSize));

    }

    private void SendDataByte(byte[] data) {
        mCustomBluetoothService.write(data);
    }

    public void listBluetoothDevice(final IPrinterSelect printerSelect) {
        if (mBluetoothAdapter == null) {
        } else {
            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                ((Activity) mContext).startActivityForResult(enableBluetooth, 0);
            }
            Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
            final List<Map<String, String>> list = new ArrayList<Map<String, String>>();
            for (BluetoothDevice device : pairedDevices) {
                Map<String, String> map = new HashMap<String, String>();
                map.put("DeviceName", device.getName());
                map.put("BDAddress", device.getAddress());
                list.add(map);
            }

            if (list.size() > 0 && list.get(0) != null && list.get(0).get("DeviceName").equals("B1ueTooth Printer")) {
                printerSelect.selectedPrinter(list.get(0).get("BDAddress"));
            } else {
                printerSelect.selectedPrinter("null");
            }
        }
    }


    public interface IPrinterSelect {
        void selectedPrinter(String address);
    }


    public interface IPrinterFinishedTask {
        void taskDone();
    }

    public interface IPrinterConnected {
        void connected();

        void notConnected(String error);
    }


}
