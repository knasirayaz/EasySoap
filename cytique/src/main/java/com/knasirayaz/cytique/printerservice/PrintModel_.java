package com.knasirayaz.cytique.printerservice;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PrintModel_ implements Serializable
{

   @SerializedName("Title")
   @Expose
   private String title;
   @SerializedName("Value")
   @Expose
   private String value;
   @SerializedName("FontSize")
   @Expose
   private String fontSize;
   @SerializedName("Alignment")
   @Expose
   private Object alignment;
   private final static long serialVersionUID = 8280772456668551040L;

   public String getTitle() {
       return title;
   }

   public void setTitle(String title) {
       this.title = title;
   }

   public String getValue() {
       return value;
   }

   public void setValue(String value) {
       this.value = value;
   }

   public String getFontSize() {
       return fontSize;
   }

   public void setFontSize(String fontSize) {
       this.fontSize = fontSize;
   }

   public Object getAlignment() {
       return alignment;
   }

   public void setAlignment(Object alignment) {
       this.alignment = alignment;
   }

}
