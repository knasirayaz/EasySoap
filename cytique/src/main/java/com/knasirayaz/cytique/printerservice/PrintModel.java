package com.knasirayaz.cytique.printerservice;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class PrintModel implements Serializable
{

    @SerializedName("PrintModel")
    @Expose
    private List<PrintModel_> printModel = null;
    private final static long serialVersionUID = -6975925239643796440L;

    public List<PrintModel_> getPrintModel() {
        return printModel;
    }

    public void setPrintModel(List<PrintModel_> printModel) {
        this.printModel = printModel;
    }

}

