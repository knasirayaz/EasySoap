package com.knasirayaz.cytique.printerservice;

import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Looper;
import android.util.Log;


import com.knasirayaz.cytique.R;
import com.knasirayaz.cytique.utils.Helper;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

public class CustomBluetoothService {
    private BluetoothAdapter mAdapter;
    private String TAG = "TAG";
    private BluetoothServiceTask customBluetoothService;
    private Context mContext;
    private Dialog mProgressDialog;

    public CustomBluetoothService() {

    }


    public CustomBluetoothService(Context mContext, BluetoothDevice device, PrintService.IPrinterConnected iPrinterCallBack) {
        this.mContext = mContext;
        mAdapter = BluetoothAdapter.getDefaultAdapter();
        if (customBluetoothService != null && customBluetoothService.getStatus() == AsyncTask.Status.RUNNING)
            customBluetoothService.cancel(true);
        customBluetoothService = null;
        customBluetoothService = new BluetoothServiceTask(device, iPrinterCallBack);
        customBluetoothService.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public void write(byte[] out) {
        customBluetoothService.write(out);
    }

    public class BluetoothServiceTask extends AsyncTask<String, Integer, String> {
        private final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
        private BluetoothSocket mmSocket;
        private BluetoothDevice mmDevice;
        private BluetoothServerSocket mmServerSocket;
        private InputStream mmInStream;
        private OutputStream mmOutStream;
        private PrintService.IPrinterConnected iPrinterCallBack;
        public BluetoothServiceTask(BluetoothDevice device, PrintService.IPrinterConnected iPrinterCallBack) {
            try {
                mmServerSocket = mAdapter.listenUsingRfcommWithServiceRecord("", MY_UUID);
                mmSocket = device.createRfcommSocketToServiceRecord(MY_UUID);
                this.mmDevice = device;
                this.iPrinterCallBack = iPrinterCallBack;
            } catch (IOException e) {
                Log.e(TAG, "listen() failed", e);
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = Helper.showCustomProgressDialog(mContext,new Dialog(mContext, R.style.dialogstyle));
            mProgressDialog.show();
        }

        protected String doInBackground(String... strings) {
            if (Looper.myLooper() == null) {
                Looper.prepare();
            }
            try {
                mAdapter.cancelDiscovery();
                mmSocket.connect();
                mmInStream = mmSocket.getInputStream();
                mmOutStream = mmSocket.getOutputStream();
                return "connected";
            } catch (Exception e) {
                return "notconnected";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mProgressDialog.dismiss();
            Log.e("", "'");
            if(s.equalsIgnoreCase("connected")){
                iPrinterCallBack.connected();
            }else{
                iPrinterCallBack.notConnected("Can not connect to printer, Try Again?");
            }

            try {
                mmSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }

        public void write(byte[] buffer) {
            try {
                mmOutStream.write(buffer);
                mmOutStream.flush();
            } catch (Exception e) {
                Log.e(TAG, "Exception during write", e);
            }
        }
    }
}
