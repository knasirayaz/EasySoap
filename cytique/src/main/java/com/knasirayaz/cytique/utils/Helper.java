package com.knasirayaz.cytique.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import com.knasirayaz.cytique.R;
import org.ksoap2.serialization.PropertyInfo;
import java.util.regex.Pattern;

public class Helper {

     public static boolean isAlphabatPresent(String text) {
        text = text.replace(" ", "");
        Pattern pattern = Pattern.compile("[A-Za-z]");
        return pattern.matcher(text).find();
    }


    public static void hideKeyboard(Activity activity) {
        View view = activity.findViewById(android.R.id.content);
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static Dialog showCustomProgressDialog(Context context, Dialog dialog) {
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.layout_progress);
        dialog.setCancelable(false);
        return dialog;
    }

    public static void showAlertDialog(Context mContext, String mTitle, String mMessage,
                                       String mPositiveText, @Nullable String mNegativeText, final IDialogCallBack callBack) {
        AlertDialog dialog;
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(mTitle);
        builder.setMessage(mMessage);
        builder.setCancelable(false);
        builder.setPositiveButton(mPositiveText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                callBack.positiveCallback("");
            }
        });
        if (mNegativeText != null) {
            builder.setNegativeButton(mNegativeText, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    callBack.negativeCallback();
                }
            });
        }
        dialog = builder.create();
        dialog.show();
    }

    public static void showDialogWithSingleListener(Context mContext, String msg, final Callback callbackListener) {
        AlertDialog dialog;
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setMessage(msg);
        builder.setCancelable(false);
        builder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                callbackListener.callback();
                dialog.dismiss();
            }
        });

        dialog = builder.create();
        dialog.show();
    }

    public static void showDialog(Context mContext, String msg) {
        AlertDialog dialog;
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setMessage(msg);

        builder.setCancelable(false);
        builder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        dialog = builder.create();
        dialog.show();
    }

    public static void showDialogWithDoubleListener(Context mContext, String msg, final IDialogCallBack callbackListener) {
        AlertDialog dialog;
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setMessage(msg);

        builder.setCancelable(false);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                callbackListener.positiveCallback("");
                dialogInterface.dismiss();
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                callbackListener.negativeCallback();
                dialog.dismiss();
            }
        });

        dialog = builder.create();
        dialog.show();
    }

    public static PropertyInfo getStringPropertyInfo(String name, String value) {
        PropertyInfo mPropertyInfo = new PropertyInfo();
        mPropertyInfo.setName(name);
        mPropertyInfo.setValue(value);
        return mPropertyInfo;
    }

    public static PropertyInfo getBooleanPropertyInfo(String name, boolean value) {
        PropertyInfo mPropertyInfo = new PropertyInfo();
        mPropertyInfo.setName(name);
        mPropertyInfo.setValue(value);
        mPropertyInfo.setType(Boolean.class);
        return mPropertyInfo;
    }

    public static PropertyInfo getLongPropertyInfo(String name, Long value) {
        PropertyInfo mPropertyInfo = new PropertyInfo();
        mPropertyInfo.setName(name);
        mPropertyInfo.setValue(value);
        return mPropertyInfo;
    }


    public interface IDialogCallBack {
        void positiveCallback(String text);
        void negativeCallback();
    }


    public interface Callback {
        public void callback();
    }

}

