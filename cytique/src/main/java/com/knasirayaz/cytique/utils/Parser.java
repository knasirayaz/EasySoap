package com.knasirayaz.cytique.utils;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.lang.reflect.Type;
import java.util.ArrayList;

public class Parser {

    public static Object objectParser(String response, Class<?> type) throws JSONException {
        JSONObject jsonArray = new JSONObject(response);
        Gson gson = new GsonBuilder().create();
        return gson.fromJson(jsonArray.toString(), type);
    }

    public static ArrayList<Object> arrayParser(String response, Class<?> type) throws JSONException {
        ArrayList<Object> categories = new ArrayList<Object>();
        JSONArray jsonArray = new JSONArray(response);

        for (int i = 0; i < jsonArray.length(); i++) {
            Gson gson = new GsonBuilder().create();
            Object model = gson.fromJson(jsonArray.get(i).toString(),(Type) type);
            categories.add(model);
        }
        return categories;
    }
}
