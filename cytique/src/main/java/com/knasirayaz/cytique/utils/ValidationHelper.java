package com.knasirayaz.cytique.utils;

import android.content.Context;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidationHelper {
    public static ValidationHelper instance = new ValidationHelper();

    public ValidationHelper() {

    }

    private static boolean isDigitPresent(String text) {
        Pattern digitPattern = Pattern.compile(".*\\d+.*");
        return digitPattern.matcher(text).matches();
    }

    private static boolean isUpperCasePresent(String text) {
        text = text.replace(" ", "");
        Pattern pattern = Pattern.compile("[A-Z]");
        return pattern.matcher(text).find();
    }

    public static boolean checkEmailFormat(String value, Context context) {
        if (value.isEmpty()) {
            return false;
        } else {
            if (!getEmailFormatStatus(value)) {
                return false;
            } else {
                return true;
            }
        }

    }

    private static boolean getEmailFormatStatus(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN =
                "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static boolean isValidEmail(String string) {
        // TODO Auto-generated method stub
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(string);
        return matcher.matches();
    }
}
