package com.knasirayaz.cytique.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.knasirayaz.cytique.R;


public class ToolBarSetup {
    private Toolbar toolbar;
    private View rightShadow, leftShadow, shareShadow;
    private ImageButton rightButton, leftButton, shareButton;
    private TextView rightText, leftText;
    private TextView title;
    private Context context;
    private RelativeLayout rlRight, rlLeft;
    private RelativeLayout rlSearch;
    String shareTitle, shareBody;
    public static int TOOLBAR_SHOW_BOTH_BUTTONS = 0;
    public static int TOOLBAR_HIDE_LEFT_SHOW_RIGHT = 1;
    public static int TOOLBAR_SHOW_LEFT_HIDE_RIGHT = 2;
    public static int TOOLBAR_HIDE_BOTH_BUTTONS = 3;


    public ToolBarSetup() {
    }

    public ToolBarSetup(Toolbar toolbar, Context context) {
        setViews(toolbar,context);
        toolbar.setVisibility(View.VISIBLE);
        hideRightText();
        hideShareButton();
    }

    private void setViews(Toolbar toolbar, Context context) {
        this.toolbar = toolbar;
        this.context = context;
        rightButton = this.toolbar.findViewById(R.id.btn_right);
        leftButton = this.toolbar.findViewById(R.id.btn_left);
        rightShadow = this.toolbar.findViewById(R.id.rightshadow);
        leftShadow = this.toolbar.findViewById(R.id.leftshadow);
        shareShadow = this.toolbar.findViewById(R.id.rightshadowshare);
        rightText = this.toolbar.findViewById(R.id.righttext);
        leftText = this.toolbar.findViewById(R.id.lefttext);
        title = this.toolbar.findViewById(R.id.toolbar_title);
        rlRight = this.toolbar.findViewById(R.id.rl_right);
        rlLeft = this.toolbar.findViewById(R.id.rl_left);
        rlSearch = this.toolbar.findViewById(R.id.rl_share);
        shareButton = this.toolbar.findViewById(R.id.btn_share);
    }

    private void hideShareButton() {
        if(rlSearch!=null)
            rlSearch.setVisibility(View.GONE);
    }

    public void hideToolbar() {
        toolbar.setVisibility(View.GONE);
    }

    public void setTitleCapsSmall() {
        title.setAllCaps(false);
    }

    public void hideLeftButton() {
        rlLeft.setVisibility(View.GONE);
        leftButton.setVisibility(View.GONE);
    }

    public String getToolBarTitle() {
        return title.getText().toString();
    }

    public void hideRightButton() {
        rightButton.setVisibility(View.GONE);
        rlRight.setVisibility(View.GONE);
    }

    public void hideRightText() {
        rightText.setVisibility(View.GONE);
    }

    public void disableRightText() {
        rightText.setEnabled(false);
        rightText.setTextColor(ContextCompat.getColor(context, android.R.color.tertiary_text_light));
    }

    public void setAlphaOnRightBtn(float value) {
        rightButton.setAlpha(value);

    }


    public void showLeftButton() {
        leftButton.setVisibility(View.VISIBLE);

        rlLeft.setVisibility(View.VISIBLE);
    }

    public ToolBarSetup showRightButton() {
        rightButton.setVisibility(View.VISIBLE);
        rlRight.setVisibility(View.VISIBLE);
        return this;
    }

    public void setTitle(String title) {
        this.title.setText(title);
        this.title.setVisibility(View.VISIBLE);
    }

    public void setTitleColor(int color) {
        title.setTextColor(ContextCompat.getColor(context, color));
    }

    public void setLeftButtonDrawable(int id) {
        leftButton.setImageResource(id);
    }

    public void setRightButtonDrawable(int id) {
        rightButton.setImageResource(id);
    }

    public void setLeftTitle(String title) {
        leftText.setText(title);
        leftButton.setVisibility(View.GONE);
        leftText.setVisibility(View.VISIBLE);
    }

    public void setRightTitle(String title) {
        rightText.setEnabled(true);
        rightText.setText(title);
        rightText.setVisibility(View.VISIBLE);
        rightButton.setVisibility(View.GONE);
    }

    @SuppressLint("ClickableViewAccessibility")
    private void setOnTouchListener(final IButtonEvents buttonEvents) {
        if(rightButton != null)
            rightButton.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN)
                        rightShadow.animate().alpha(1f).setDuration(100).setInterpolator(new DecelerateInterpolator());
                    else if (event.getAction() == MotionEvent.ACTION_UP) {
                        rightShadow.animate().alpha(0f).setDuration(100).setInterpolator(new DecelerateInterpolator());
                        buttonEvents.rightEvent();
                    }

                    return true;
                }
            });

        if(leftButton != null)
            leftButton.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN)
                        leftShadow.animate().alpha(1f).setDuration(100).setInterpolator(new DecelerateInterpolator());
                    else if (event.getAction() == MotionEvent.ACTION_UP) {
                        leftShadow.animate().alpha(0f).setDuration(100).setInterpolator(new DecelerateInterpolator());
                        buttonEvents.leftEvent();
                    }

                    return true;
                }
            });
        if(shareButton!=null)
            shareButton.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN)
                        shareShadow.animate().alpha(1f).setDuration(100).setInterpolator(new DecelerateInterpolator());
                    else if (event.getAction() == MotionEvent.ACTION_UP) {
                        shareShadow.animate().alpha(0f).setDuration(100).setInterpolator(new DecelerateInterpolator());
                        buttonEvents.rightEvent();
                    }
                    return true;
                }
            });
    }



    private void setTextOnClickListener(final IButtonEvents buttonEvents) {
        rightText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonEvents.rightEvent();
            }
        });
        leftText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonEvents.leftEvent();
            }
        });
    }


    public void setBackgroudColor(int colorResId) {
        toolbar.setBackgroundColor(ContextCompat.getColor(context, colorResId));
    }

    private void changeRightTextClick(boolean isEnabled) {
        rightText.setAlpha(isEnabled ? 1 : 0.5f);
        rightText.setEnabled(isEnabled);
    }

    public static ToolBarSetup setToolBarDefault(Activity activity, Toolbar toolBar,
                                                 String title, IButtonEvents events, int drawableRight) {
        ToolBarSetup toolBarSetup = new ToolBarSetup(toolBar, activity);
        toolBarSetup.showLeftButton();
        toolBarSetup.showRightButton();
        toolBarSetup.setOnTouchListener(events);
        toolBarSetup.setTitle(title);
        toolBarSetup.hideLeftButton();
        toolBarSetup.setTextOnClickListener(events);
        toolBarSetup.changeRightTextClick(true);
        toolBarSetup.setRightButtonDrawable(drawableRight);
        return toolBarSetup;
    }


    public ToolBarSetup setToolBarDefault(Activity activity, Toolbar toolBar, String title, int mType) {
        ToolBarSetup toolBarSetup = new ToolBarSetup(toolBar, activity);
        toolBarSetup.setTitle(title);
        if(mType == 0){
            toolBarSetup.showLeftButton();
            toolBarSetup.showRightButton();
        }else if(mType == 1){
            toolBarSetup.hideLeftButton();
            toolBarSetup.showRightButton();
        }else if(mType == 2){
            toolBarSetup.showLeftButton();
            toolBarSetup.hideRightButton();
        }else if(mType == 3){
            toolBarSetup.hideLeftButton();
            toolBarSetup.hideRightButton();
        }

        toolBarSetup.changeRightTextClick(true);
        return toolBarSetup;
    }

    public ToolBarSetup setToolBarDefault(Activity activity, Toolbar toolBar, String title, int mType, IButtonEvents buttons) {
        ToolBarSetup toolBarSetup = new ToolBarSetup(toolBar, activity);
        toolBarSetup.setTitle(title);
        toolBarSetup.setOnTouchListener(buttons);

        if(mType == TOOLBAR_SHOW_BOTH_BUTTONS){
            toolBarSetup.showLeftButton();
            toolBarSetup.showRightButton();
        }else if(mType == TOOLBAR_HIDE_LEFT_SHOW_RIGHT){
            toolBarSetup.hideLeftButton();
            toolBarSetup.showRightButton();
        }else if(mType == TOOLBAR_SHOW_LEFT_HIDE_RIGHT){
            toolBarSetup.showLeftButton();
            toolBarSetup.hideRightButton();
        }else if(mType == TOOLBAR_HIDE_BOTH_BUTTONS){
            toolBarSetup.hideLeftButton();
            toolBarSetup.hideRightButton();
        }


        return toolBarSetup;
    }


    private void showShareButton() {
        rlSearch.setVisibility(View.VISIBLE);
    }

}


