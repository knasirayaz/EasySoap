package com.knasirayaz.cytique.utils;

public interface IButtonEvents {
    public void leftEvent();
    public void rightEvent();
}