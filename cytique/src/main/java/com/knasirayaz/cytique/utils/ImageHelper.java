package com.knasirayaz.cytique.utils;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ImageHelper {
    public static final String IMAGE_PATH = "photo.png";
    public int requestCamera = 5678;
    public int selectFile = 5679;

    private Activity activity;
    private Fragment fragment;
    private ImageView imageView;
    private String imagePath;
    private String imageBase64;

    public ImageHelper(Activity activity, ImageView imageView, int codeCamera, int codeFile) {
        this.activity = activity;
        this.imageView = imageView;
        this.requestCamera = codeCamera;
        this.selectFile = codeFile;
    }

    public ImageHelper(Fragment fragment, ImageView imageView, int codeCamera, int codeFile) {
        this.activity = fragment.getActivity();
        this.fragment = fragment;
        this.imageView = imageView;
        this.requestCamera = codeCamera;
        this.selectFile = codeFile;
    }

    public void openCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (fragment != null)
            fragment.startActivityForResult(intent, requestCamera);
        else
            activity.startActivityForResult(intent, requestCamera);
    }

    public void openGallery() {
        Intent intent = new Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        if (fragment != null)
            fragment.startActivityForResult(
                    Intent.createChooser(intent, "Select Image"),
                    selectFile);
        else
            activity.startActivityForResult(
                    Intent.createChooser(intent, "Select Image"),
                    selectFile);
    }

    public String getBase64ImageFromBitmap(Bitmap bm){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
        byte[] b = baos.toByteArray();
        return Base64.encodeToString(b, Base64.DEFAULT);
    }
    public void setImageFromCameraResult(Intent data) {
        Bundle extras = data.getExtras();
        Bitmap imageBitmap = (Bitmap) extras.get("data");
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        imageBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        setBase64EncodedImage(stream);
        imageView.setImageBitmap(imageBitmap);
      /*  Uri tempUri = getImageUri(imageBitmap);
        File finalFile = new File(getRealPathFromURI(tempUri));
        imagePath = finalFile.getPath();*/
    }

    public Uri getImageUri(Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(activity.getContentResolver(),
                inImage, "img_", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = activity.getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    public String saveImageToInternalStorage(Bitmap image) {
        File pictureFile = getOutputMediaFile();
        if (pictureFile == null) {
            return null;
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.PNG, 70, fos);
            fos.close();
            return pictureFile.toString();
        } catch (Exception e) {
            Log.d("ERROR ", e.getMessage().toString());
        }
        return null;
    }

    private File getOutputMediaFile() {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory()
                + "/Android/data/"
                + activity.getPackageName()
                + "/Files");

        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmm").format(new Date());
        File mediaFile;
        String mImageName = "MI_" + timeStamp + ".jpg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    private Bitmap rotateImage(Bitmap bm, int i) {
        int width = bm.getWidth();
        int height = bm.getHeight();

        Matrix matrix;
        matrix = new Matrix();
        matrix.postRotate(i);

        return Bitmap.createBitmap(bm, 0, 0,
                width, height, matrix, true);
    }

    private void setBase64EncodedImage(ByteArrayOutputStream stream) {
        imageBase64 = Base64.encodeToString(stream.toByteArray(), Base64.DEFAULT);
    }

    public String getImagePath() {
        return imagePath;
    }

    public String getImageBase64() {
        return imageBase64;
    }

    private Bitmap decodeFile(File f) {
        try {
            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);

            // The new size we want to scale to
            final int REQUIRED_SIZE = 70;

            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_SIZE &&
                    o.outHeight / scale / 2 >= REQUIRED_SIZE) {
                scale *= 2;
            }

            // Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {
        }
        return null;
    }

    public Bitmap resizeBitmapByScale(
            Bitmap bitmap, float scale, boolean recycle) {
        int width = Math.round(bitmap.getWidth() * scale);
        int height = Math.round(bitmap.getHeight() * scale);
        if (width == bitmap.getWidth()
                && height == bitmap.getHeight()) return bitmap;
        Bitmap target = Bitmap.createBitmap(width, height, getConfig(bitmap));
        Canvas canvas = new Canvas(target);
        canvas.scale(scale, scale);
        Paint paint = new Paint(Paint.FILTER_BITMAP_FLAG | Paint.DITHER_FLAG);
        canvas.drawBitmap(bitmap, 0, 0, paint);
        if (recycle) bitmap.recycle();
        return target;
    }

    private Bitmap.Config getConfig(Bitmap bitmap) {
        Bitmap.Config config = bitmap.getConfig();
        if (config == null) {
            config = Bitmap.Config.ARGB_8888;
        }
        return config;
    }


}