package com.knasirayaz.cytique.utils;

public class NumberUtils {
    boolean isValid;
    String number;

    public NumberUtils(boolean isValid, String number) {
        this.isValid = isValid;
        this.number = number;
    }

    public static NumberUtils validatePKMobileNumber(String number){
        if (number.startsWith("0")) {
            return startsWith0(number);
        }
        if (number.startsWith("92")){
            return new NumberUtils(isValidPKMobile(number),number);
        }
        if (number.startsWith("+92")){
            return startsWithPlus(number);
        }
        if (number.startsWith("3")){
            return startsWith3(number);
        }
        return new NumberUtils(false,number);
    }

    private static NumberUtils startsWith0(String number){
        if (number.length() == 11){
            number = "92" + number.substring(1);
            if (isValidPKMobile(number)) {
                return new NumberUtils(true,number);
            }
        }
        return new NumberUtils(false,number);
    }
    private static NumberUtils startsWith3(String number){
        if (number.length() == 10){
            number = "92" + number;
            if (isValidPKMobile(number)) {
                return new NumberUtils(true,number);
            }
        }
        return new NumberUtils(false,number);
    }


    private static NumberUtils startsWithPlus(String number){
        if (number.length() == 13){
            number = number.substring(1);
//                Log.e("Number",number);
            if (isValidPKMobile(number)) {
                return new NumberUtils(true,number);
            }
        }
        return new NumberUtils(false,number);
    }

    private static boolean isValidPKMobile(String number) {

        //return patternPKMobile.matcher(number).matches();
        return (number.startsWith("923") && number.length() == 12);


    }

    public boolean isValid() {
        return isValid;
    }

    public void setValid(boolean valid) {
        isValid = valid;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}

