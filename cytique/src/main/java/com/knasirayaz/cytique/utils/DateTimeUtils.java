package com.knasirayaz.cytique.utils;


import android.support.annotation.Nullable;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateTimeUtils {

    public static String YEAR_MONTH_DAY_HOURS_MIN_SECONDS = "yyyy/MM/dd HH:mm:ss";
    public static String DAY_HOURS = "ddHH";
    public static String MONTH_DAY_YEAR = "MM/dd/yyyy";
    public static String YEAR_DAY_MONTH = "yyyy/dd/MM";
    public static String DAY_MONTH_YEAR = "dd/MM/yyyy";
    public static String YEAR_MONTH_DAY = "yyyy-MM-dd";

    //04/30/2018


    public static String convertDateTime(String format) {
        DateFormat dateFormat = new SimpleDateFormat(format);
        Calendar cal = Calendar.getInstance();
        return dateFormat.format(cal.getTime());
    }


    public static String convertDateTime(String Date, String formatFrom, String formatTo) throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat(formatFrom);
        DateFormat dateFormatTo = new SimpleDateFormat(formatTo);

        Calendar cal = Calendar.getInstance();
        java.util.Date datse = dateFormat.parse(Date);
        cal.setTime(datse);
        return dateFormatTo.format(cal.getTime());
        // System.out.println(datse); // Sat Jan 02 00:00:00 GMT 2010

    }


    public static Long convertDateTimeIntoMilis(String Date, String formatFrom) throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat(formatFrom);

        Calendar cal = Calendar.getInstance();
        java.util.Date datse = dateFormat.parse(Date);
        cal.setTime(datse);
        return cal.getTimeInMillis();
    }

    public static Boolean validDate(String mStartDate, String mEndDate, String mErrorMessage) throws ParseException {
        mErrorMessage = null;
        //Date startdate = new Date(mStartDate);
        Date startdate = new Date(convertDateTimeIntoMilis(mStartDate,DateTimeUtils.YEAR_MONTH_DAY));
        Date enddate = new Date(convertDateTimeIntoMilis(mEndDate,DateTimeUtils.YEAR_MONTH_DAY));

        //Date enddate = new Date();
        if (startdate.after(enddate) ) {
            mErrorMessage = "End Date could not be after Start Date.";
        } else if (startdate.after(new Date(System.currentTimeMillis()))) {
            mErrorMessage = "Start Date could not be after Today.";
        } else if (enddate.after(new Date(System.currentTimeMillis()))) {
            mErrorMessage = "End Date could not be after Today.";
        }
        if (mErrorMessage == null) {
            return true;
        }

        return false;
    }



    @Nullable
    public static String convertDate(long date, String format){
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(date);
        SimpleDateFormat forma2t = new SimpleDateFormat(format);
        return forma2t.format(c.getTime());
    }

    public static String convertMilisIntoDateTime(String dateInMilliseconds) {
        String dateFormat  = "MM/dd/yyyy hh:mm:ss";
        return android.text.format.DateFormat.format(dateFormat, Long.parseLong(dateInMilliseconds)).toString();
    }
}
