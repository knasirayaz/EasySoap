package com.knasirayaz.cytique.utils;

import android.content.Context;

public class SessionClass {

    private  final String TOKEN = "Token";
    private  String mUserLogin = "userLogin";
    private  String DEVICE_ID = "DeviceID";
    private  String USER_ID = "UserID";
    private static SessionClass mInstance;


    private SessionClass() {
    }

    public static SessionClass getInstance() {
        if (mInstance == null)
            mInstance = new SessionClass();
        return mInstance;
    }

    public Boolean getUserLogin(Context mContext) {
        return new PrefManager(mContext).getBooleanByKey(mUserLogin);
    }

    public void setUserLogin(Boolean userLogin, Context mContext) {
        new PrefManager(mContext).setBooleanForKey(userLogin,mUserLogin);
    }

    public String getToken(Context mContext) {
        return new PrefManager(mContext).getStringByKey(TOKEN);
    }
    public void setToken(Context mContext, String token) {
        new PrefManager(mContext).setStringForKey(token,TOKEN);
    }

    public void setUserid(Context mContext, String mUserID) {
        new PrefManager(mContext).setStringForKey(mUserID, DEVICE_ID);
    }

    public void setDeviceId(Context mContext, String mDeviceID) {
        new PrefManager(mContext).setStringForKey(mDeviceID, DEVICE_ID);
    }

    public String getUserid(Context mContext) {
        return new PrefManager(mContext).getStringByKey(USER_ID);
    }

    public String getDeviceId(Context mContext) {
        return new PrefManager(mContext).getStringByKey(USER_ID);
    }
}
