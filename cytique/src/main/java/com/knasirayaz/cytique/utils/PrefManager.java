package com.knasirayaz.cytique.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Collection;


/**
 * Created by nasir on 3/13/2018.
 */

public class PrefManager {
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;

    // shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "welcome";

    public PrefManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }


    public void clearPreferences() {
        SharedPreferences.Editor editor = pref.edit();
        editor.clear();
        editor.apply();
    }


    public void setBooleanForKey(Boolean val, String key) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(key, val);
        editor.commit();
    }

    public void setStringForKey(String val, String key) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(key, val);
        editor.commit();
    }

    public void setDoubleForKey(Float val, String key) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putFloat(key, val);
        editor.commit();
    }


    public void setIntegerForKey(int val, String key) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt(key, val);
        editor.commit();
    }

    public Boolean getBooleanByKey(String key) {
        if (pref.contains(key)) {
            return pref.getBoolean(key, false);
        }
        return false;
    }

    public String getStringByKey(String key){
        if (pref.contains(key)) {
            return pref.getString(key, "");
        }
        return null;
    }

    public int getIntegerByKey(String key){
        if (pref.contains(key)) {
            return pref.getInt(key, 0);
        }
        return 0;
    }

    public void saveCollectionTOSharedPref(Object collection, String key) {
        String value = new Gson().toJson(collection);
        setStringForKey(value, key);
    }


    public Collection getCollectionFromSharedPref(String key){
        String value = getStringByKey(key);
        GsonBuilder gsonb = new GsonBuilder();
        Gson gson = gsonb.create();
        Collection collection  = gson.fromJson(value, Collection.class);
        return collection;
    }

    public void saveObjectInSharedPref(Object obj, String key) {
        Gson gson = new Gson();
        String json = gson.toJson(obj);
        setStringForKey(json, key);
    }

    public Object getObjectFromSharedPref(String key,Class toCast) {
        Gson gson = new Gson();
        String json = getStringByKey(key);
        if (json!=null&&!json.equals("")){
            return gson.fromJson(json,toCast);
        }else{
            return null;
        }
    }


    public boolean isContains(String key){
        return pref.contains(key);
    }

}

