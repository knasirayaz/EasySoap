package com.knasirayaz.cytique.utils;

import android.content.Context;
import android.os.AsyncTask;


import org.ksoap2.serialization.PropertyInfo;

import java.util.ArrayList;

public class Templates {

    //Template Api Call Method
   /* public static void apiCall(Context mContext,
                               String url,
                               String methodName,
                               ArrayList<PropertyInfo> mPropertyInfo,
                               final IWebserviceCallback iWebserviceCallback) {

        WebClient webserviceClient = new WebClient(mContext,
                url,
                methodName,
                "",
                true,
                0,
                mPropertyInfo,
                new IWebserviceCallback() {


                    @Override
                    public void onResponseReceived(String jsonResponse) {
                        iWebserviceCallback.onResponseReceived(jsonResponse);
                    }

                    @Override
                    public void onResponseNotReceived() {
                        iWebserviceCallback.onResponseNotReceived();
                    }
                });

        webserviceClient.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }*/

    /* Template Custom Dialog Method */

    /*

      public static void showCustomDialog(Context mContext, String mTitle, String mMessage,
                                               String mPositiveText, String mNegativeText, String mHint,
                                               final IPaymentEvents callBack) {

        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_inputfield_radio);

        final TextView tvTitle = dialog.findViewById(R.id.tv_title);
        final TextView tvMessage = dialog.findViewById(R.id.tv_message);
        final EditText etInput = dialog.findViewById(R.id.et_input);
        final EditText etname = dialog.findViewById(R.id.et_name);
        final EditText etcity = dialog.findViewById(R.id.et_city);
        final EditText etemail = dialog.findViewById(R.id.et_email);

        final Button btRight = dialog.findViewById(R.id.bt_right);
        final Button btLeft = dialog.findViewById(R.id.bt_left);
        final RadioButton rbSIP = dialog.findViewById(R.id.rb_sip);
        final RadioButton rbWallet = dialog.findViewById(R.id.rb_wallet);

        tvTitle.setText(mTitle);
        tvMessage.setText(mMessage);
        btRight.setText(mPositiveText);
        btLeft.setText(mNegativeText);
        etInput.setHint(mHint);
        btLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        rbSIP.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    etInput.setVisibility(View.VISIBLE);
                    etname.setVisibility(View.VISIBLE);
                    etcity.setVisibility(View.VISIBLE);
                    etemail.setVisibility(View.VISIBLE);
                } else {
                    etInput.setVisibility(View.INVISIBLE);
                    etname.setVisibility(View.INVISIBLE);
                    etcity.setVisibility(View.INVISIBLE);
                    etemail.setVisibility(View.INVISIBLE);

                }
            }
        });

        btRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rbSIP.isChecked()) {
                    callBack.positiveCallback(etInput.getText().toString(), etname.getText().toString(), etcity.getText().toString(), etemail.getText().toString(), 2);
                } else {
                    callBack.positiveCallback("", "", "", "", 1);

                }
                dialog.dismiss();

            }
        });
        dialog.show();
    }
     */
}
